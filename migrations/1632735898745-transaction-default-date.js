"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
exports.transactionDefaultDate1632735898745 = void 0;
var typeorm_1 = require("typeorm");
var transactionDefaultDate1632735898745 = /** @class */ (function () {
    function transactionDefaultDate1632735898745() {
    }
    transactionDefaultDate1632735898745.prototype.up = function (queryRunner) {
        return __awaiter(this, void 0, void 0, function () {
            var newColumn;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        newColumn = new typeorm_1.TableColumn();
                        newColumn["default"] = '(current_date())';
                        newColumn.type = 'date';
                        newColumn.isNullable = false;
                        newColumn.name = 'just_date';
                        return [4 /*yield*/, queryRunner.addColumn('transactions', newColumn)];
                    case 1:
                        _a.sent();
                        return [4 /*yield*/, queryRunner.query("\n            UPDATE transactions t SET just_date=date(t.date);\n    ")];
                    case 2:
                        _a.sent();
                        return [4 /*yield*/, queryRunner.dropColumn('transactions', 'date')];
                    case 3:
                        _a.sent();
                        return [4 /*yield*/, queryRunner.renameColumn('transactions', 'just_date', 'date')];
                    case 4:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    transactionDefaultDate1632735898745.prototype.down = function (queryRunner) {
        return __awaiter(this, void 0, void 0, function () {
            var query, newColumn;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        query = 'ALTER TABLE `transactions` CHANGE `date` `just_date` date NOT NULL DEFAULT (curdate())';
                        return [4 /*yield*/, queryRunner.query(query)];
                    case 1:
                        _a.sent();
                        newColumn = new typeorm_1.TableColumn();
                        newColumn["default"] = '(current_timestamp())';
                        newColumn.type = 'timestamp';
                        newColumn.name = 'date';
                        return [4 /*yield*/, queryRunner.addColumn('transactions', newColumn)];
                    case 2:
                        _a.sent();
                        return [4 /*yield*/, queryRunner.query("\n            UPDATE transactions t SET date=timestamp(t.just_date);\n    ")];
                    case 3:
                        _a.sent();
                        return [4 /*yield*/, queryRunner.dropColumn('transactions', 'just_date')];
                    case 4:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    return transactionDefaultDate1632735898745;
}());
exports.transactionDefaultDate1632735898745 = transactionDefaultDate1632735898745;
