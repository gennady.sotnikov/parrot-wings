import { MigrationInterface, QueryRunner } from 'typeorm';

export class transactions1632655706113 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`
        CREATE TABLE \`transactions\` (
            \`id\` int NOT NULL AUTO_INCREMENT,
            \`date\` datetime NOT NULL,
            \`userId\` int NOT NULL,
            \`amount\` int NOT NULL,
            \`balance\` int NOT NULL,
            \`userWhoSendId\` int NOT NULL,
            \`balanceWhoSend\` int NOT NULL,
            PRIMARY KEY (\`id\`),
            KEY \`user_fk_idx\` (\`userId\`) /*!80000 INVISIBLE */,
            KEY \`user_who_get_fk_idx\` (\`userWhoSendId\`),
            CONSTRAINT \`user_fk\` FOREIGN KEY (\`userId\`) REFERENCES \`users\` (\`id\`),
            CONSTRAINT \`user_who_send_fk\` FOREIGN KEY (\`userWhoSendId\`) REFERENCES \`users\` (\`id\`)
          ) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb3
      `);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`DROP TABLE \`transactions\``);
  }
}
