import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

export class transactionDefaultDate1632735898745 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    const newColumn = new TableColumn();
    newColumn.default = '(current_date())';
    newColumn.type = 'date';
    newColumn.isNullable = false;
    newColumn.name = 'just_date';
    await queryRunner.addColumn('transactions', newColumn);
    await queryRunner.query(`
            UPDATE transactions t SET just_date=date(t.date);
    `);
    await queryRunner.dropColumn('transactions', 'date');
    await queryRunner.renameColumn('transactions', 'just_date', 'date');
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    const query =
      'ALTER TABLE `transactions` CHANGE `date` `just_date` date NOT NULL DEFAULT (curdate())';
    await queryRunner.query(query);
    const newColumn = new TableColumn();
    newColumn.default = '(current_timestamp())';
    newColumn.type = 'timestamp';
    newColumn.name = 'date';
    await queryRunner.addColumn('transactions', newColumn);
    await queryRunner.query(`
            UPDATE transactions t SET date=timestamp(t.just_date);
    `);
    await queryRunner.dropColumn('transactions', 'just_date');
  }
}
