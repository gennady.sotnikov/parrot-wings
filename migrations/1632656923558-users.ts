import { MigrationInterface, QueryRunner } from 'typeorm';

export class PostRefactoring1632655063433 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`
        CREATE TABLE \`users\` (
                \`username\` varchar(16) NOT NULL,
                \`email\` varchar(255) DEFAULT NULL,
                \`password\` varchar(100) NOT NULL,
                \`balance\` int NOT NULL,
                \`id\` int NOT NULL AUTO_INCREMENT,
                PRIMARY KEY (\`id\`),
                UNIQUE KEY \`username_UNIQUE\` (\`username\`),
                UNIQUE KEY \`email_UNIQUE\` (\`email\`)
            ) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb3
    `);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query('DROP TABLE `users`');
  }
}
