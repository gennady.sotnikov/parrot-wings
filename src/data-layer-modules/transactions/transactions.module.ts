import { MiddlewareConsumer, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthModule } from 'src/auth/auth.module';
import { JWTGuardMiddleware } from 'src/auth/jwt-guard.middleware';
import { UsersModule } from 'src/data-layer-modules/users/users.module';
import { Transaction } from '../entities/transaction.entity';
import { TransactionsController } from './transactions.controller';
import { TransactionsService } from './transactions.service';

@Module({
  imports: [AuthModule, UsersModule, TypeOrmModule.forFeature([Transaction])],
  providers: [TransactionsService],
  controllers: [TransactionsController],
})
export class TransactionsModule {
  configure(consumer: MiddlewareConsumer) {
    consumer.apply(JWTGuardMiddleware).forRoutes(TransactionsController);
  }
}
