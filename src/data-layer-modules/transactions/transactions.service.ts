import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Transaction } from 'src/data-layer-modules/entities/transaction.entity';
import {
  ITransactionDto,
  ITransactionView,
} from 'src/data-layer-modules/models/transaction';
import { Repository } from 'typeorm';
import { UsersService } from '../users/users.service';

@Injectable()
export class TransactionsService {
  static readonly createTransactionErrors = [
    'user not found',
    'balance exceeded',
    'You cannot send wings to yourself',
  ];
  constructor(
    @InjectRepository(Transaction)
    private transactionRepository: Repository<Transaction>,
    private userService: UsersService,
  ) {}

  async transactions(requestingUserId: number): Promise<ITransactionView[]> {
    const transactionsInDb2 = await this.getUsersTransactions(requestingUserId);

    return transactionsInDb2
      .map((el) => this.getTransactionView(el, requestingUserId))
      .sort((a, b) => {
        if (a.id > b.id) return -1;
        if (a.id < b.id) return 1;
      });
  }

  private getTransactionView(
    t: Transaction,
    requestingUserId: number,
  ): ITransactionView {
    const isSent = requestingUserId === t.userWhoSend.id;
    return {
      id: t.id,
      date: t.date,
      username: isSent ? t.user.username : t.userWhoSend.username,
      balance: isSent ? t.balanceWhoSend : t.balance,
      amount: (isSent ? -1 : 1) * t.amount,
    };
  }

  async getUsersTransactions(userId: number): Promise<Transaction[]> {
    const transactions = await this.transactionRepository.find({
      relations: ['user', 'userWhoSend'],
      where: [{ userId: userId }, { userWhoSendId: userId }],
    });

    return transactions;
  }

  async create(
    newValue: ITransactionDto,
    userWhoSendId: number,
  ): Promise<ITransactionView> {
    const userWhoSend = await this.userService.findById(userWhoSendId);
    if (userWhoSend.balance < newValue.amount) {
      throw new Error(TransactionsService.createTransactionErrors[0]);
    }
    const userWhoGet = (
      await this.userService.searchByUsername(newValue.name)
    )[0];
    if (!userWhoGet) {
      throw new Error(TransactionsService.createTransactionErrors[1]);
    }
    if (userWhoGet.id === userWhoSend.id) {
      throw new Error(TransactionsService.createTransactionErrors[2]);
    }
    const newBalance = userWhoSend.balance - newValue.amount;
    const newBalanceGet = userWhoGet.balance + newValue.amount;
    await this.userService.update(userWhoSend.id, {
      ...userWhoSend,
      balance: newBalance,
    });
    await this.userService.update(userWhoGet.id, {
      ...userWhoGet,
      balance: newBalanceGet,
    });
    const newTransactionInDB = await this.transactionRepository.save({
      amount: newValue.amount,
      balance: newBalanceGet,
      balanceWhoSend: newBalance,
      user: userWhoGet,
      userWhoSend: userWhoSend,
    });
    return this.getTransactionView(newTransactionInDB, userWhoSendId);
  }
}
