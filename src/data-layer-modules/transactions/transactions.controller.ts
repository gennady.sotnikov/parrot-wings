import {
  Body,
  Controller,
  Get,
  HttpException,
  Post,
  Request,
} from '@nestjs/common';
import { JWTData } from 'src/auth/types';
import { errorTexts } from 'src/constants';
import { ITransactionDto } from 'src/data-layer-modules/models/transaction';
import { TransactionsService } from 'src/data-layer-modules/transactions/transactions.service';

@Controller('api/protected')
export class TransactionsController {
  constructor(private transactionsService: TransactionsService) {}

  @Post('/transactions')
  async createTransaction(@Body() body: ITransactionDto, @Request() req) {
    try {
      const result = await this.transactionsService.create(
        body,
        (req.user as JWTData).id,
      );
      return { trans_token: result };
    } catch (err) {
      if (TransactionsService.createTransactionErrors.includes(err.message)) {
        throw new HttpException(err.message, 400);
      }
      throw new HttpException(errorTexts.internal, 500);
    }
  }

  @Get('/transactions')
  async getTransactions(@Request() req) {
    const res = await this.transactionsService.transactions(
      (req.user as JWTData).id,
    );
    return { trans_token: res };
  }
}
