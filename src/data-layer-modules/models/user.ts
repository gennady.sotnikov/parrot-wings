export interface CreateUserReqBody {
  username: string;
  password: string;
  email: string;
}

export interface IUserBase {
  username: string;
  password: string;
  email: string;
  balance: number;
}

export interface IFindUser extends Partial<IUserBase> {
  id?: number | number[];
}

export interface IUser extends IUserBase {
  id: number;
}

export interface IUserAuthResult {
  id: number;
  username: string;
  email: string;
  balance: number;
}
