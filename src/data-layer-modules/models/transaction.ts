import { User } from 'src/data-layer-modules/entities/user.entity';

export interface ITransactionDto {
  name: string;
  amount: number;
}

export interface ITransactionBase {
  amount: number;
  balance: number;
  balanceWhoSend: number;
  date: Date;
  userWhoSend: User;
  user: User;
}

export interface ITransaction extends ITransactionBase {
  id: number;
}

export interface ITransactionView {
  id: number;
  username: string;
  date: Date;
  balance: number;
  amount: number;
}
