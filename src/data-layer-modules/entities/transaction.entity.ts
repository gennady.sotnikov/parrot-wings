import { ITransaction } from 'src/data-layer-modules/models/transaction';
import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { User } from './user.entity';

@Entity('transactions')
export class Transaction implements ITransaction {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: 'date' })
  date: Date;

  @Column({ type: 'int' })
  balance: number;

  @Column({ type: 'int' })
  balanceWhoSend: number;

  @Column({ type: 'int' })
  amount: number;

  @ManyToOne(() => User, (user) => user.recieviedTransactions)
  @JoinColumn()
  user: User;

  @Column({ type: 'int' })
  userId?: number;

  @ManyToOne(() => User, (user) => user.sentTransactions)
  @JoinColumn()
  userWhoSend: User;

  @Column({ type: 'int' })
  userWhoSendId?: number;
}
