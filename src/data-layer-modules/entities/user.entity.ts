import { IUser } from 'src/data-layer-modules/models/user';
import { Transaction } from './transaction.entity';
import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';

@Entity('users')
export class User implements IUser {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ unique: true })
  username: string;

  @Column()
  password: string;

  @Column({ type: 'int' })
  balance: number;

  @Column({ unique: true })
  email: string;

  @OneToMany(() => Transaction, (transaction) => transaction.user)
  recieviedTransactions?: Transaction[];

  @OneToMany(() => Transaction, (transaction) => transaction.userWhoSend)
  sentTransactions?: Transaction[];
}
