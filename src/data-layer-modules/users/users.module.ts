import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from 'src/data-layer-modules/entities/user.entity';
import { UsersService } from './users.service';

const userRep = TypeOrmModule.forFeature([User]);

@Module({
  imports: [userRep],
  providers: [UsersService],
  exports: [UsersService],
})
export class UsersModule {}
