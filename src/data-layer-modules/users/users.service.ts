import { Injectable } from '@nestjs/common';
import { User } from '../entities/user.entity';
import { Like, Not, Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { CreateUserReqBody, IUserBase } from '../models/user';
import { initialBalance } from 'src/constants';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User)
    private usersRepository: Repository<User>,
  ) {}

  async create({ email, username, password }: CreateUserReqBody) {
    const res = await this.usersRepository.save({
      balance: initialBalance,
      email,
      username,
      password,
    });
    return res;
  }

  searchByEmail(email: string) {
    return this.usersRepository.find({ email });
  }

  searchByUsername(username: string) {
    return this.usersRepository.find({ username });
  }

  search(searchStr: string, excludeId: number) {
    return this.usersRepository.find({
      select: ['id', 'username'],
      where: {
        username: Like(searchStr),
        id: Not(excludeId),
      },
    });
  }

  async findById(id: number) {
    const res = await this.usersRepository.findOne(id);
    return res;
  }

  async getTransactionsOfUser(id: number) {
    const res = await this.usersRepository.findOne(id, {
      relations: ['sentTransactions', 'recieviedTransactions'],
    });
    const { sentTransactions, recieviedTransactions } = res;
    return sentTransactions && recieviedTransactions
      ? [...sentTransactions, ...recieviedTransactions]
      : null;
  }

  async update(id: number, newValue: IUserBase) {
    const res = await this.usersRepository.update(id, newValue);
    return res;
  }
}
