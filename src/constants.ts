export const initialBalance = 500;

export const errorTexts = {
  internal: 'Internal Server Error',
  noStr: 'No search string',
  userExists: 'A user with that email already exists',
  userPasswordRequired: 'You must send username and password',
  unAuth: 'UnauthorizedException',
  invalidCreds: 'Invalid login or password',
};
