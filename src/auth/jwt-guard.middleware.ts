import { Injectable, NestMiddleware } from '@nestjs/common';
import { Request, Response, NextFunction } from 'express';
import { errorTexts } from 'src/constants';
import { JwtStrategy } from './jwt.strategy';

@Injectable()
export class JWTGuardMiddleware implements NestMiddleware {
  constructor(private jwtStrategy: JwtStrategy) {}
  use(req: Request, res: Response, next: NextFunction) {
    this.jwtStrategy.setCallbacks({
      fail: () => {
        res.status(401).end(errorTexts.unAuth);
        this.jwtStrategy.clearCallbacks();
        return;
      },
      error: (e) => {
        res.status(500).end(errorTexts.internal);
        this.jwtStrategy.clearCallbacks();
        return;
      },
      success: (authRes) => {
        req.user = authRes;
        this.jwtStrategy.clearCallbacks();
        next();
      },
    });
    this.jwtStrategy.authenticate(req);
  }
}
