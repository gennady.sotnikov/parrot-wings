import { Strategy } from 'passport-local';
import { Injectable } from '@nestjs/common';
import { AuthService } from './auth.service';
import { PWStrategy } from './strategy.abstract';

@Injectable()
export class LocalStrategy extends PWStrategy(Strategy) {
  constructor(private authService: AuthService) {
    super({ usernameField: 'email' });
  }

  async validate(email: string, password: string): Promise<any> {
    if (email && password) {
      const user = await this.authService.validateUser(email, password);
      return user;
    }
  }
}
