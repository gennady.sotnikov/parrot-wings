import { Injectable, NestMiddleware } from '@nestjs/common';
import { Request, Response, NextFunction } from 'express';
import { errorTexts } from 'src/constants';
import { LocalStrategy } from './local.strategy';

@Injectable()
export class LocalGuardMiddleware implements NestMiddleware {
  constructor(private localStrategy: LocalStrategy) {}
  use(req: Request, res: Response, next: NextFunction) {
    const { email, password } = req.body;
    if (!email || !password) {
      res.status(400).send(errorTexts.userPasswordRequired);
      return;
    }
    this.localStrategy.setCallbacks({
      fail: () => {
        res.status(401).send(errorTexts.invalidCreds);
        this.localStrategy.clearCallbacks();
        return;
      },
      error: (e) => {
        res.status(500).end(errorTexts.internal);
        this.localStrategy.clearCallbacks();
        return;
      },
      success: (authRes) => {
        req.user = authRes;
        this.localStrategy.clearCallbacks();
        next();
      },
    });
    this.localStrategy.authenticate(req);
  }
}
