import { Body, Controller, HttpException, Post, Request } from '@nestjs/common';
import { UsersService } from '../data-layer-modules/users/users.service';
import * as bcrypt from 'bcrypt';
import { CreateUserReqBody } from 'src/data-layer-modules/models/user';
import { AuthService } from './auth.service';
import { errorTexts } from 'src/constants';

@Controller()
export class AuthController {
  constructor(
    private userService: UsersService,
    private authService: AuthService,
  ) {}
  @Post('/users')
  async users(@Body() body: Partial<CreateUserReqBody>) {
    const { password, username, email } = body;
    if (username && password && email) {
      const rounds = process.env.PASSWORD_HASH_ROUNDS;
      if (!rounds) throw new HttpException(errorTexts.internal, 500);
      const hashedPassword = await bcrypt.hash(password, +rounds);
      try {
        const newUser = await this.userService.create({
          ...body,
          password: hashedPassword,
        } as CreateUserReqBody);
        return await this.authService.login(newUser);
      } catch (e) {
        throw new HttpException(errorTexts.userExists, 400);
      }
    }
    throw new HttpException(errorTexts.userPasswordRequired, 400);
  }

  @Post('sessions/create')
  async create(@Request() req) {
    const loginRes = await this.authService.login(req.user);
    return loginRes;
  }
}
