import { MiddlewareConsumer, Module } from '@nestjs/common';
import { PassportModule } from '@nestjs/passport';
import { UsersModule } from 'src/data-layer-modules/users/users.module';
import { AuthService } from './auth.service';
import { JWTGuardMiddleware } from './jwt-guard.middleware';
import { JwtStrategy } from './jwt.strategy';
import { LocalGuardMiddleware } from './local-guard.middleware';
import { JwtModule } from '@nestjs/jwt';
import { AuthController } from './auth.controller';
import { LocalStrategy } from './local.strategy';
import { UsersController } from './users.controller';

const jwtModule = JwtModule.register({});

@Module({
  imports: [UsersModule, PassportModule, jwtModule],
  providers: [
    AuthService,
    JWTGuardMiddleware,
    JwtStrategy,
    LocalGuardMiddleware,
    LocalStrategy,
  ],
  controllers: [AuthController, UsersController],
  exports: [jwtModule, JWTGuardMiddleware, JwtStrategy],
})
export class AuthModule {
  configure(consumer: MiddlewareConsumer) {
    consumer.apply(LocalGuardMiddleware).forRoutes('/sessions/create');
    consumer.apply(JWTGuardMiddleware).forRoutes(UsersController);
  }
}
