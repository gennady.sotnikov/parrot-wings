import {
  Body,
  Controller,
  Post,
  Request,
  Get,
  HttpException,
} from '@nestjs/common';
import { errorTexts } from 'src/constants';
import { UsersService } from '../data-layer-modules/users/users.service';

@Controller('api/protected')
export class UsersController {
  constructor(private userService: UsersService) {}

  @Get('/user-info')
  async getUserInfo(@Request() req) {
    const user = await this.userService.findById(req.user.id);
    if (user) {
      const { id, username, email, balance } = user;
      return { user_info_token: { id, email, name: username, balance } };
    }
    throw new HttpException(errorTexts.internal, 500);
  }

  @Post('/users/list')
  async getUsersList(@Body() body, @Request() req) {
    if (!body.filter) {
      return req.status(401 /* from api discription */).end(errorTexts.noStr);
    } else {
      const result = await this.userService.search(body.filter, req.user.id);
      return result.map(({ username, id }) => ({ name: username, id }));
    }
  }
}
