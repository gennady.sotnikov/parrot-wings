import { PassportStrategy } from '@nestjs/passport';
import { IUserAuthResult } from 'src/data-layer-modules/models/user';

type SuccessCallback = (authResult: IUserAuthResult) => unknown;
type ErrorCallback = (e: Error) => unknown;
type FailCallback = () => unknown;

type CallbacksArg = {
  success: SuccessCallback;
  error: ErrorCallback;
  fail: FailCallback;
};

export const PWStrategy = (Strategy, name?: string | undefined) =>
  class AbstractStrategy
    extends PassportStrategy(Strategy, name)
    implements CallbacksArg
  {
    success: SuccessCallback;
    error: ErrorCallback;
    fail: FailCallback;
    setCallbacks(arg: CallbacksArg) {
      const { error, success, fail } = arg;
      this.error = error;
      this.success = success;
      this.fail = fail;
    }

    clearCallbacks() {
      this.error = null;
      this.success = null;
      this.fail = null;
    }
  };
