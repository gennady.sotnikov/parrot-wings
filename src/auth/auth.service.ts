import { Injectable } from '@nestjs/common';
import { UsersService } from '../data-layer-modules/users/users.service';
import { JwtService } from '@nestjs/jwt';
import * as bcrypt from 'bcrypt';
import { IUserAuthResult } from 'src/data-layer-modules/models/user';
import { JWTData } from './types';

@Injectable()
export class AuthService {
  constructor(
    private usersService: UsersService,
    private jwtService: JwtService,
  ) {}

  async validateUser(
    email: string,
    password: string,
  ): Promise<IUserAuthResult> {
    const user = (await this.usersService.searchByEmail(email))[0];
    if (!user) return null;
    const isMatch = await bcrypt.compare(password, user.password);
    if (user && isMatch) {
      const { password, ...result } = user;
      return result;
    }
    return null;
  }

  async login(user: IUserAuthResult) {
    const payload: JWTData = { id: user.id };
    return {
      id_token: this.jwtService.sign(payload, {
        secret: process.env.JWT_KEY,
        expiresIn: '365d',
      }),
    };
  }
}
