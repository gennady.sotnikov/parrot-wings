import { ExtractJwt, Strategy } from 'passport-jwt';
import { Injectable } from '@nestjs/common';
import { IUserAuthResult } from 'src/data-layer-modules/models/user';
import { JWTData } from './types';
import { PWStrategy } from './strategy.abstract';

type SuccessCallback = (authResult: IUserAuthResult) => unknown;
type ErrorCallback = (e: Error) => unknown;
type FailCallback = () => unknown;

type CallbacksArg = {
  success: SuccessCallback;
  error: ErrorCallback;
  fail: FailCallback;
};
@Injectable()
export class JwtStrategy extends PWStrategy(Strategy) {
  error: ErrorCallback;
  success: SuccessCallback;
  fail: FailCallback;
  constructor() {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: process.env.JWT_KEY,
    });
  }

  setCallbacks(arg: CallbacksArg) {
    const { error, success, fail } = arg;
    this.error = error;
    this.success = success;
    this.fail = fail;
  }

  clearCallbacks() {
    this.error = null;
    this.success = null;
    this.fail = null;
  }

  async validate(payload: IUserAuthResult): Promise<JWTData> {
    return { id: payload.id };
  }
}
