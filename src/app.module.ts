import { Module } from '@nestjs/common';
import { TransactionsModule } from './data-layer-modules/transactions/transactions.module';
import { UsersModule } from './data-layer-modules/users/users.module';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports: [TransactionsModule, UsersModule, TypeOrmModule.forRoot()],
})
export class AppModule {}
